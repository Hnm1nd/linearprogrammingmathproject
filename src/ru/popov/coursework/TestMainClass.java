package ru.popov.coursework;

import ru.popov.coursework.interfaces.AlgorithmListener;
import ru.popov.coursework.math.data.*;
import ru.popov.coursework.math.optimization.method.linear.simplex.Simplex;
import ru.popov.coursework.math.optimization.method.linear.simplex.data.SimplexTable;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TestMainClass {

    private static AlgorithmListener<SimplexTable, AlgorithmOutputData> listener = new AlgorithmListener<SimplexTable, AlgorithmOutputData>() {
        @Override
        public void onEvent(int eventType, Object table) {
        }

        @Override
        public void onStart() {
        }

        @Override
        public void onIteration(int iterationsCount, SimplexTable table) {
            if (table == null) {
                log("table is null");
                return;
            }
            log(table.toString());
            log("");
            log("-----------------");
            log("");
        }

        @Override
        public void onFinished(AlgorithmOutputData algorithmOutputData) {
            if (algorithmOutputData.getResultValues() == null) {
                log("result is null");
                return;
            }
            log("The result: ");
            for (Map.Entry<String, Variable> entry : algorithmOutputData.getResultValues().entrySet()) {
                log("   " + entry.getKey() + " = " + entry.getValue().getFractionStr());
            }
        }

        @Override
        public void onError(Throwable throwable) {

        }
    };

    public static void main(String[] args) {

        test0();
//        test1();
//        test2();
//        test3();
//        test4();
//        test5();
//        test8();
//        test9();
//        test10();
//        test11();
    }

    private static void test0() {
        TargetFunction targetFunction = TargetFunction.parseEquation("f = 3x1 + 4x2 -> max");

        List<Limitation> limitations = new ArrayList<>();

        limitations.add(Limitation.parseLimitation("x1 + x2 <= 550"));
        limitations.add(Limitation.parseLimitation("2x1 + 3x2 <= 1200"));
        limitations.add(Limitation.parseLimitation("12x1 + 30x2 <= 9600"));

        Objective objective = new Objective(targetFunction, limitations);

        startSimplex(objective);
    }

    private static void test1() {
        Objective objective = new Objective();
        objective.setEquation("f = 2x1 - x2 + 3x3 - 2x4 + x5 -> max");
        objective.addLimitation("-x1 + x2 + x3 == 1");
        objective.addLimitation("x1 - x2 + x4 == 1");
        objective.addLimitation("x1 - 2x2 + x5 == 2");

        startSimplex(objective);
    }

    private static void test2() {
        Objective objective = new Objective();
        objective.setEquation("f = -2x1 -x2 -> max");
        objective.addLimitation("-x1 -x2 -x3 == -2");
        objective.addLimitation("-4x2 - 2x5 >= -10");
        objective.addLimitation("-2x2 -x3 -x4 == -1");

        startSimplex(objective);
    }

    private static void test3() {
        Objective objective = new Objective();
        objective.setEquation("f = x1 + 2x2 -> max");
        objective.addLimitation("x1 + 2x2 <= 6");
        objective.addLimitation("2x1 + x2 <= 8");
        objective.addLimitation("x2 <= 2");

        startSimplex(objective);
    }

    private static void test5() {
        TargetFunction targetFunction = TargetFunction.parseEquation("f = -3x1 + x2 + 4x3 -> max");


        List<Limitation> limitations = new ArrayList<>();
        limitations.add(Limitation.parseLimitation("-x2 + x3 + x4 == 1"));
        limitations.add(Limitation.parseLimitation("-5x1 + x2 + x3 == 2"));
        limitations.add(Limitation.parseLimitation("-8x1 + x2 + 2x3 - x5 == 3"));

        Objective objective = new Objective(targetFunction, limitations);

        startSimplex(objective);
    }

    private static void test6() {
        TargetFunction targetFunction = TargetFunction.parseEquation("f = 2x1 + x2 - 2x3 -> min");

        List<Limitation> limitations = new ArrayList<>();
        limitations.add(Limitation.parseLimitation("x1 + x2 - x3 >= 8"));
        limitations.add(Limitation.parseLimitation("x1 - x2 + 2x3 >= 2"));
        limitations.add(Limitation.parseLimitation("-2x1 - 8x2 + 3x3 >= 1"));

        Objective objective = new Objective(targetFunction, limitations);

        startSimplex(objective);
    }

    private static void test7() {
        Map<String, Variable> equationVariables = new HashMap<>();
        equationVariables.put("x1", new Variable(-3, 1));
        equationVariables.put("x2", new Variable(1, 1));
        equationVariables.put("x3", new Variable(4, 1));
        TargetFunction.EquationType type = TargetFunction.EquationType.TENDS_MAX;


        TargetFunction targetFunction = new TargetFunction(type, equationVariables);

        Map<String, Variable> limitationVariables1 = new HashMap<>();
        limitationVariables1.put("x1", new Variable(0, 1));
        limitationVariables1.put("x2", new Variable(-1, 1));
        limitationVariables1.put("x3", new Variable(1, 1));
        Limitation.LimitationType limitationType1 = Limitation.LimitationType.LESS_THAN;
        Limitation limitation1 = new Limitation(limitationVariables1, limitationType1, new Variable(1, 1));

        Map<String, Variable> limitationVariables2 = new HashMap<>();
        limitationVariables2.put("x1", new Variable(-5, 1));
        limitationVariables2.put("x2", new Variable(1, 1));
        limitationVariables2.put("x3", new Variable(1, 1));
        Limitation.LimitationType limitationType2 = Limitation.LimitationType.EQUAL;
        Limitation limitation2 = new Limitation(limitationVariables2, limitationType2, new Variable(2, 1));

        Map<String, Variable> limitationVariables3 = new HashMap<>();
        limitationVariables3.put("x1", new Variable(-8, 1));
        limitationVariables3.put("x2", new Variable(1, 1));
        limitationVariables3.put("x3", new Variable(2, 1));
        Limitation.LimitationType limitationType3 = Limitation.LimitationType.MORE_THAN;
        Limitation limitation3 = new Limitation(limitationVariables3, limitationType3, new Variable(3, 1));

        List<Limitation> limitations = new ArrayList<>();
        limitations.add(limitation1);
        limitations.add(limitation2);
        limitations.add(limitation3);

        Objective objective = new Objective(targetFunction, limitations);

        startSimplex(objective);
    }

    private static void test8() {
        Objective objective = new Objective();
        objective.setEquation("f = 2x1 + 4x2 -> max");
        objective.addLimitation("x1 + x2 <= 24");
        objective.addLimitation("x1 + 2x2 <= 40");
        objective.addLimitation("3x1 + x2 <= 52");

        startSimplex(objective);
    }

    private static void test9() {
        Objective objective = new Objective();
        objective.setEquation("4x1 + 5x2 + 7x3 + 8x4 + " +
                "5y1 + 6y2 + 3y3 + 11y4 + " +
                "8z1 + 9z2 + 6z3 + 3z4 -> min");
        objective.addLimitation("x1 + x2 + x3 + x4 == 300");
        objective.addLimitation("y1 + y2 + y3 + y4 == 470");
        objective.addLimitation("z1 + z2 + z3 + z4 == 230");
        objective.addLimitation("x1 + y1 + z1 == 220");
        objective.addLimitation("x2 + y2 + z2 == 180");
        objective.addLimitation("x3 + y3 + z3 == 340");
        objective.addLimitation("x4 + y4 + z4 == 260");

        startSimplex(objective);
    }

    private static void test10() {
        Objective objective = new Objective();
        objective.setEquation("2x1 + 5x2 -> max");

        objective.addLimitation("3x1 + 3x2 <= 30");
        objective.addLimitation("x1 + 2x2 <= 16");
        objective.addLimitation("3x1 + x2 <= 25");

        startSimplex(objective);
    }

    private static void test11() {
        Objective objective = new Objective();
        objective.setEquation("2x1 + x2 -> max");
        objective.addLimitation("2x1 + x2 <= 40");
        objective.addLimitation("2x1 + 2x2 <= 40");
        objective.addLimitation("x1 + 2x2 <= 36");

        startSimplex(objective);
    }

    private static void startSimplex(Objective objective) {
        objective.update();

        Simplex simplex = new Simplex(objective);
        simplex.setListener(listener);
        simplex.count();
    }

    private static void log(String msg) {
        System.out.println(msg);
    }
}
