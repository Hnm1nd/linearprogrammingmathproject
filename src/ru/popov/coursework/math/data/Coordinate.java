package ru.popov.coursework.math.data;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by bronetemkin on 26.03.17.
 */
public class Coordinate {

    private int x, y;

    public Coordinate(int x, int y){
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public void setX(int x){
        this.x = x;
    }

    public void setY(int y){
        this.y = y;
    }

    @Override
    public String toString() {
        return x + ":" + y;
    }

    @Override
    public boolean equals(Object obj) {
        if(obj instanceof Coordinate){
            return ((Coordinate) obj).getX() == getX() && ((Coordinate) obj).getY() == getY();
        }
        return super.equals(obj);
    }

    public static Coordinate parseCoordinate(String str){
        Pattern pattern = Pattern.compile("\\s*(\\d+):(\\d+)\\s*");
        Matcher matcher = pattern.matcher(str);
        if(matcher.find()) {
            return new Coordinate(Integer.valueOf(matcher.group(1)), Integer.valueOf(matcher.group(2)));
        }else
            return null;
    }

    public static void log(String msg){
        System.out.println(msg);
    }
}
