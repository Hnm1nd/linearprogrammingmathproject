package ru.popov.coursework.math.data;

import java.util.*;

public class Objective {

    private TargetFunction targetFunction;
    private List<Limitation> limitations;
    private List<String> variableList;

    public Objective(){}

    public Objective(TargetFunction targetFunction, List<Limitation> limitations){
        this.targetFunction = targetFunction;
        this.limitations = limitations;
        update();
    }

    public void setTargetFunction(TargetFunction targetFunction){
        this.targetFunction = targetFunction;
    }

    public void setEquation(String equation){
        setTargetFunction(TargetFunction.parseEquation(equation));
    }

    public TargetFunction getTargetFunction(){
        return targetFunction;
    }

    public List<Limitation> getLimitations(){
        return limitations;
    }

    public List<String> getVariableList(){
        return variableList;
    }

    public String getVariableName(int position){
        return variableList.get(position);
    }

    public void addLimitation(Limitation limitation){
        if(limitations == null)
            limitations = new ArrayList<>();
        limitations.add(limitation);
    }

    public void addLimitation(String limitation){
        addLimitation(Limitation.parseLimitation(limitation));
    }

    void removeExtra(List<String> extra){
        for(String s:extra){
            int index = variableList.indexOf(s);
            if(index > -1)
                variableList.remove(index);
        }
    }

    public void update(){
        variableList = countVariables(this);
        addRemainingVariables();
    }

    private void addRemainingVariables(){
        for(Limitation limitation:limitations){
            for(String variableName:variableList){
                if(limitation.getVariableByName(variableName) == null)
                    limitation.setVariable(variableName, new Variable(0, 1));
            }
        }
    }

    public Objective copyObjective(){
        List<Limitation> list = new ArrayList<>();
        for(Limitation limitation:limitations)
            list.add(limitation.copyLimitation());
        return new Objective(new TargetFunction(targetFunction.getType(), new HashMap<>(targetFunction.getEquationVariables())), list);
    }

    public static List<String> countVariables(Objective objective){
        return countVariables(objective.getTargetFunction(), objective.getLimitations());
    }

    public static List<String> countVariables(TargetFunction targetFunction, List<Limitation> limitations){
        Set<String> varSet = new HashSet<>();
        for(Map.Entry<String, Variable> entry: targetFunction.getEquationVariables().entrySet()){
            varSet.add(entry.getKey());
        }
        for(Limitation limitation:limitations){
            for(Map.Entry<String, Variable> entry:limitation.getLimitationVariables().entrySet()){
                varSet.add(entry.getKey());
            }
        }
        List<String> result = new ArrayList<>();
        result.addAll(varSet);
        return result;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("TargetFunction vars: ");
        sb.append("\n");
        for(Map.Entry<String, Variable> entry: targetFunction.getEquationVariables().entrySet()){
            sb.append("     ");
            sb.append(entry.getKey());
            sb.append(" = ");
            sb.append(entry.getValue().getFractionStr());
            sb.append("\n");
        }

        sb.append("---------");
        sb.append("\n");

        sb.append("Limitation vars: ");
        sb.append("\n");
        for(Limitation limitation:limitations){
            sb.append("     ---------");
            sb.append("\n");
            for(Map.Entry<String, Variable> entry:limitation.getLimitationVariables().entrySet()){
                sb.append("     ");
                sb.append(entry.getKey());
                sb.append(" = ");
                sb.append(entry.getValue().getFractionStr());
                sb.append("\n");
            }
            sb.append("     limit ");
            sb.append(limitation.getType());
            sb.append(" ");
            sb.append(limitation.getValue().getFractionStr());
            sb.append("\n");
        }
        sb.append("---------");
        sb.append("\n");
        sb.append("Variable list: ");
        sb.append("\n");
        for(String s:variableList){
            sb.append("     ");
            sb.append(s);
            sb.append("\n");
        }
        return sb.toString();
    }
}
