package ru.popov.coursework.math.data;

import java.util.AbstractMap;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Limitation {

    private LimitationType type;
    private Variable value;
    private Map<String, Variable> limitationVariables;
    private String basicVariableName;

    private Limitation(){
        limitationVariables = new HashMap<>();
        value = new Variable(0, 1);
        type = LimitationType.EQUAL;
    }

    public Limitation(Map<String, Variable> limitationVariables, LimitationType type, Variable value){
        this.limitationVariables = limitationVariables;
        this.type = type;
        this.value = value;
    }

    public void setBasicVariableName(String basicVariableName){
        this.basicVariableName = basicVariableName;
    }

    public String getBasicVariableName(){
        return basicVariableName;
    }

    public void setVariable(String name, Variable variable){
        limitationVariables.put(name, variable);
    }

    public LimitationType getType(){
        return type;
    }

    public Variable getValue(){
        return value;
    }

    public Map<String, Variable> getLimitationVariables(){
        return limitationVariables;
    }

    public Variable getVariableByName(String name){
        return limitationVariables.get(name);
    }

    private void flip(){
        for(Map.Entry<String, Variable> entry:limitationVariables.entrySet())
            limitationVariables.put(entry.getKey(), Variable.multiply(entry.getValue(), new Variable(-1, 1)));
        value = Variable.multiply(value, new Variable(-1, 1));
        switch (type){
            case LESS_THAN:{
                type = LimitationType.MORE_THAN;
                break;
            }
            case MORE_THAN:{
                type = LimitationType.LESS_THAN;
                break;
            }
        }
    }

    public Limitation copyLimitation(){
        return new Limitation(new HashMap<>(limitationVariables), type, new Variable(value.getNumerator(), value.getDenominator()));
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();

        boolean isFirst = true;

        for(Map.Entry<String, Variable> variableEntry:limitationVariables.entrySet()) {
            if (isFirst)
                isFirst = false;
            else if (variableEntry.getValue().getNumerator() > 0)
                stringBuilder.append(" +");
            else
                stringBuilder.append(" -");

            stringBuilder.append(" ");

            if (variableEntry.getValue().getNumerator() != 1 && variableEntry.getValue().getNumerator() != -1)
                stringBuilder.append(variableEntry.getValue().getFractionStr());

            stringBuilder.append(variableEntry.getKey());
        }

        switch (type){
            case EQUAL:{
                stringBuilder.append(" == ");
                break;
            }
            case LESS_THAN:{
                stringBuilder.append(" <= ");
                break;
            }
            case MORE_THAN:{
                stringBuilder.append(" >= ");
                break;
            }
        }

        stringBuilder.append(value.getFractionStr());

        return stringBuilder.toString();
    }

    public static Limitation parseLimitation(String limitation) {
        Limitation result = new Limitation();
        Pattern pattern = Pattern.compile("\\s*([+-]?\\d*\\D\\d.*)+\\s*(>=|<=|==)\\s*([+-]?\\d*)");
        Matcher matcher = pattern.matcher(limitation);
        if (!matcher.find()) {
//            System.out.println("Matcher can't find limitation");
            return null;
        }


//        System.out.println(matcher.groupCount());
//        for (int i = 0; i <= matcher.groupCount(); i++)
//            System.out.println("group [" + i + "]: " + matcher.group(i));


        result.limitationVariables = findVariables(matcher.group(1));
        switch (matcher.group(2).toLowerCase()) {
            case ">=": {
                result.type = LimitationType.MORE_THAN;
                break;
            }
            case "<=": {
                result.type = LimitationType.LESS_THAN;
                break;
            }
            case "==":{
                result.type = LimitationType.EQUAL;
                break;
            }
        }

        result.value = new Variable(Integer.valueOf(matcher.group(3)), 1);

        if(result.value.getNumerator() < 0)
            result.flip();

//        System.out.println("result: " + result.toString());
        return result;
    }

    public static boolean canParse(String limitation){
        Pattern pattern = Pattern.compile("\\s*([+-]?\\d*\\D\\d.*)+\\s*(>=|<=|==)\\s*([+-]?\\d*)");
        Matcher matcher = pattern.matcher(limitation);
        return matcher.find();
    }

    private static Map<String, Variable> findVariables(String variables){
        Map<String, Variable> result = new HashMap<>();

//        int iterations = 0;

        String vars = variables.replaceAll(" ", "");

        Pattern pattern1 = Pattern.compile("(([+-])?(\\d*)?(\\D*)(\\d*)).*");

        while(!vars.isEmpty()) {
//            if(iterations > 10)
//                break;
            Matcher matcher1 = pattern1.matcher(vars);

            /*
             1 - (+|-|null)
             2 - variable modifier
             3 - variable letter
             4 - variable count
             */

            if (matcher1.find()) {
//                for (int i = 0; i <= matcher1.groupCount(); i++)
//                    System.out.println("group[" + i + "] = " + matcher1.group(i));

                String sign, letter, modifierStr;
                int modifier, count;

                sign = matcher1.group(2);
                modifierStr = matcher1.group(3);
                letter = matcher1.group(4);
                count = Integer.valueOf(matcher1.group(5));

                if(modifierStr == null || modifierStr.isEmpty())
                    modifier = 1;
                else
                    modifier = Integer.valueOf(modifierStr);

                if(sign != null && sign.equals("-"))
                    modifier *= -1;

                result.put(letter + count, new Variable(modifier, 1));

                vars = vars.replace(matcher1.group(1), "");
            }else
                break;

        }

        return result;
    }

    public static Map.Entry<String, Variable> findFirstVariable(String variable) {
        String vars = variable.replaceAll(" ", "");

        Pattern pattern = Pattern.compile("(([+-])?(\\d*)?(\\D*)(\\d*)).*");
        Matcher matcher = pattern.matcher(vars);

        if (!matcher.find())
            return null;

        String sign, letter, modifierStr;
        int modifier, count;

        sign = matcher.group(2);
        modifierStr = matcher.group(3);
        letter = matcher.group(4);
        count = Integer.valueOf(matcher.group(5));

        if (modifierStr == null || modifierStr.isEmpty())
            modifier = 1;
        else
            modifier = Integer.valueOf(modifierStr);

        if (sign != null && sign.equals("-"))
            modifier *= -1;

        return new AbstractMap.SimpleEntry<>(letter + count, new Variable(modifier, 1));
    }

    public enum LimitationType{
        MORE_THAN, LESS_THAN, EQUAL
    }

}
