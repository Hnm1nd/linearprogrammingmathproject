package ru.popov.coursework.math.data;

import java.util.Map;

/**
 * Created by bronetemkin on 25.03.17.
 */
public class AlgorithmOutputData {

    private Map<String, Variable> resultValues;

    public AlgorithmOutputData(Map<String, Variable> resultValues){
        this.resultValues = resultValues;
    }

    public Map<String, Variable> getResultValues(){
        return resultValues;
    }

}
