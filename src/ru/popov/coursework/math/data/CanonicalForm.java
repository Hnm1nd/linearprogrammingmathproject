package ru.popov.coursework.math.data;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by bronetemkin on 26.03.17.
 */
public class CanonicalForm extends Objective {

    public static final int FIRST_BASIC_VARIABLE_NUMBER = 1;
    public static final String BASIC_VARIABLE_LETTER = "u";

    private List<String> basicVariables;

    public CanonicalForm(TargetFunction targetFunction, List<Limitation> limitations, List<String> basicVariables) {
        super(targetFunction, limitations);
        this.basicVariables = basicVariables;
        removeExtra(basicVariables);
    }

    public List<String> getBasicVariableNames(){
        return basicVariables;
    }

    public String getBasicVariable(int position){
        return basicVariables.get(position);
    }

    public Variable getVariableCoefficientByName(String varName){
        Variable result = new Variable(0, 1);
        if(getTargetFunction().getEquationVariables().containsKey(varName))
            result = getTargetFunction().getEquationVariables().get(varName);
        return result;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder(super.toString());
        sb.append("---------");
        sb.append("\n");

        sb.append("Basic vars: ");
        sb.append("\n");
        for(String s:basicVariables){
            sb.append("     ");
            sb.append(s);
            sb.append("\n");
        }
        return sb.toString();
    }

    public static CanonicalForm makeCanonicalForm(Objective data){

        int variableNumber = FIRST_BASIC_VARIABLE_NUMBER;
        for(int i = 0; i < data.getLimitations().size(); i++){
            Limitation limitation = data.getLimitations().get(i);
            String varName = BASIC_VARIABLE_LETTER + variableNumber;
            switch (limitation.getType()){
                case LESS_THAN:{
//                    basicVariables.add(i, varName);
                    limitation.setVariable(varName, new Variable(1, 1));
                    variableNumber++;
//                    limitations.add(addNewVariable(limitation, varName, new Variable(1, 1)));
                    break;
                }
                case MORE_THAN:{
//                    basicVariables.add(i, varName);
                    limitation.setVariable(varName, new Variable(-1, 1));
                    variableNumber++;
//                    limitations.add(addNewVariable(limitation, varName, new Variable(-1, 1)));
                    break;
                }
            }
        }

        List<String> basicVariables;
        Map<String, Integer> basicVars = findBasicVariables(data);

//        for(Map.Entry<String, Integer> entry:basicVars.entrySet())
//            log(entry.getKey() +" = " + entry.getValue());

        basicVariables = new ArrayList<>();
        for(Map.Entry<String, Integer> var:basicVars.entrySet()) {
            if(var.getValue() >= basicVariables.size())
                for(int i = basicVariables.size(); i <= var.getValue(); i++)
                    basicVariables.add(null);
            if(basicVariables.get(var.getValue()) == null || basicVariables.get(var.getValue()).isEmpty())
                basicVariables.set(var.getValue(), var.getKey());
        }

        return new CanonicalForm(data.getTargetFunction(), data.getLimitations(), basicVariables);
    }

    public static Map<String, Integer> findBasicVariables(Objective objective){
        Map<String, Integer> result = new HashMap<>();
        objective.update();
        for(String str:objective.getVariableList()) {
//            System.out.println("Variable name = " + str);
            boolean haveUnit = false, canExit = false;
            int basicVarPosition = -1;
            for (int i = 0; i < objective.getLimitations().size(); i++) {
                if(canExit)
                    continue;
                Variable variable = objective.getLimitations().get(i).getVariableByName(str);
//                System.out.println("if " + variable.getNumerator() + " == 1 && !haveUnit = " + Boolean.toString(!haveUnit));

                switch (variable.getNumerator()){
                    case 1:{
                        if(haveUnit) {
                            canExit = true;
                            basicVarPosition = -1;
                            continue;
                        }else {
                            basicVarPosition = i;
                            haveUnit = true;
                        }
                        break;
                    }
                    case 0:{
                        if(i == objective.getLimitations().size() - 1 && !haveUnit)
                            canExit = true;
                        break;
                    }
                    default:{
                        basicVarPosition = -1;
//                        System.out.println("can exit");
                        canExit = true;
                    }
                }

                /*
                if(variable.getNumerator() == 1 && nullInPrevious) {
                    result.put(str, i);
                    nullInPrevious = false;
                }
                */
            }
            if(basicVarPosition != -1) {
//                System.out.println(str + " put on "+ basicVarPosition);
                result.put(str, basicVarPosition);
            }
//            System.out.println("----------------------");
        }

//        System.out.println("basic variables");
//        for(Map.Entry<String, Integer> entry:result.entrySet())
//            System.out.println("varName = " + entry.getKey() + ", limitation position = " + entry.getValue());
        return result;
    }

    public static void log(String msg){
        System.out.println(msg);
    }

}
