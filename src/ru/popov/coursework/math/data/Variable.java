package ru.popov.coursework.math.data;

/**
 * Created by bronetemkin on 26.03.17.
 */
public class Variable {

    private int numerator, denominator;

    public Variable(int numerator, int denominator){
        this.numerator = numerator;
        this.denominator = denominator;
    }

    public int getNumerator(){
        return numerator;
    }

    public int getDenominator(){
        return denominator;
    }

    public int getInteger(){
        return getInteger(numerator, denominator);
    }

    public Double getDouble(){
        return getDouble(numerator, denominator);
    }

    private void setNumerator(int numerator){
        this.numerator = numerator;
    }

    private void setDenominator(int denominator){
        this.denominator = denominator;
    }

    public boolean isInteger(){
        return isInteger(numerator, denominator);
    }

    public boolean isFraction(){
        return denominator != 1;
    }

    public String getFractionStr(){
        return getFractionStr(this);
    }

    public Variable minimizeFraction(){
        Variable var = minimizeFraction(numerator, denominator);
        this.numerator = var.numerator;
        this.denominator = var.denominator;
        return this;
    }

    public Variable flip(){
        Variable var = flip(this);
        this.numerator = var.numerator;
        this.denominator = var.denominator;
        return var;
    }

    public static Variable minimizeFraction(int numerator, int denominator){
        int gcd = greatestCommonDivisor(numerator, denominator);
//        log("GreatestCommonDivisor = " + gcd);
        if(gcd < 0)
            gcd *= -1;

        return new Variable(numerator / gcd, denominator / gcd);
    }

    public static Variable minimizeFraction(Variable variable){
        return minimizeFraction(variable.numerator, variable.denominator);
    }

    public static Variable multiply(Variable a, Variable b){
        if(a != null && b != null) {

            int num = a.getNumerator() * b.getNumerator(),
                    denum = a.getDenominator() * b.getDenominator();

            Variable result = new Variable(num, denum);
            return result.minimizeFraction();
        }else
            return null;
    }

    public static Variable divide(Variable a, Variable b){
//        log("divide " + a.getFractionStr() + " on " + b.getFractionStr());
        return multiply(a, Variable.flip(b));
    }

    public static int greatestCommonDivisor(int x0, int x1){
        return x1 == 0 ? x0 : greatestCommonDivisor(x1,x0 % x1);
    }

    public static int leastCommonMultiple(int x0, int x1){
        return x0 / greatestCommonDivisor(x0,x1) * x1;
    }

    public static int leastCommonMultiple(int x0, int x1, int x2){
        return x0/ leastCommonMultiple(x1, x2);
    }

    public static int getInteger(int numerator, int denominator){
        return numerator / denominator;
    }

    public static Double getDouble(int numerator, int denominator){
        return (double) numerator / denominator;
    }

    public static boolean isInteger(int numerator, int denominator){
        return (denominator == 0) || (numerator % denominator == 0);
    }

    public static Variable flip(Variable variable){

        int num = variable.numerator, denum = variable.denominator, resultN, resultD;

//        log("num = " + num + ", denum = " + denum);


        if(num < 0){
//            log(num + " < 0");
            resultN = num * -1;
            resultD = denum * -1;
        }else {
            resultN = num;
            resultD = denum;
        }

//        log("result num = " + resultN + ", denum = " + resultD);
        return new Variable(resultD, resultN);
    }

    public static String getFractionStr(Variable variable){
        if(variable != null) {
            Variable var = Variable.minimizeFraction(variable);
            if (var.getDenominator() == 1) {
                return String.valueOf(var.getNumerator());
            } else {
                return var.getNumerator() + "/" + var.getDenominator();
            }
        }else
            return null;
    }

    public static Variable plus(Variable a, Variable b){
        Variable[] arr = makeEqualDenominators(a, b);

        return new Variable(arr[0].getNumerator() + arr[1].getNumerator(), arr[0].getDenominator());
    }

    public static Variable minus(Variable a, Variable b){
        Variable[] arr = makeEqualDenominators(a, b);

        return new Variable(arr[0].getNumerator() - arr[1].getNumerator(), arr[0].getDenominator());
    }

    public static Variable[] makeEqualDenominators(Variable a, Variable b){
        Variable a1 = a, b1 = b;
        int lcm = leastCommonMultiple(a1.getDenominator(), b1.getDenominator());

        if(a1.getDenominator() != lcm) {
            a1 = Variable.multiplyFraction(lcm / a1.getDenominator(), a1);
        }

        if(b1.getDenominator() != lcm) {
            b1 = Variable.multiplyFraction(lcm / b1.getDenominator(), b1);
        }

        Variable[] result = new Variable[2];
        result[0] = a1;
        result[1] = b1;
        return result;
    }

    public static Variable multiplyFraction(int multiplier, Variable variable){
        return new Variable(variable.getNumerator() * multiplier, variable.getDenominator() * multiplier);
    }

    public static boolean isLess(Variable a, Variable b){
        Variable[] vars = makeEqualDenominators(a, b);

        return vars[0].getNumerator() < vars[1].getNumerator();
    }

    public static void log(String msg){
        System.out.println(msg);
    }

}
