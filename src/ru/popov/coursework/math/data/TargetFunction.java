package ru.popov.coursework.math.data;

import java.util.AbstractMap;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by User on 25.03.2017.
 */
public class TargetFunction {

    private EquationType type;
    private Map<String, Variable> equationVariables;

    private TargetFunction(){
        equationVariables = new HashMap<>();
        type = EquationType.TENDS_MAX;
    }

    public TargetFunction(EquationType type, Map<String, Variable> equationVariables){
        this.type = type;
        this.equationVariables = equationVariables;
    }

    public EquationType getType(){
        return type;
    }

    public Map<String, Variable> getEquationVariables(){
        return equationVariables;
    }

    public void maximize(){
        if(type == EquationType.TENDS_MAX)
            return;

        for(Map.Entry<String, Variable> entry:equationVariables.entrySet()){
            Variable value = entry.getValue();
            equationVariables.put(entry.getKey(), new Variable(value.getNumerator() * -1, value.getDenominator()));
        }

        type = EquationType.TENDS_MAX;
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder("f =");

        boolean isFirst = true;

        for(Map.Entry<String, Variable> variableEntry:equationVariables.entrySet()) {
            if (isFirst)
                isFirst = false;
            else if (variableEntry.getValue().getNumerator() > 0)
                stringBuilder.append(" +");
            else
                stringBuilder.append(" -");

            stringBuilder.append(" ");

            if (variableEntry.getValue().getNumerator() != 1 && variableEntry.getValue().getNumerator() != -1)
                stringBuilder.append(variableEntry.getValue().getFractionStr());

            stringBuilder.append(variableEntry.getKey());
        }

        stringBuilder.append(" -> ");

        switch (type){
            case TENDS_MAX:{
                stringBuilder.append("max");
                break;
            }
            case TENDS_MIN:{
                stringBuilder.append("min");
                break;
            }
        }

        return stringBuilder.toString();
    }

    public static TargetFunction parseEquation(String equation) {
        TargetFunction result = new TargetFunction();
        Pattern pattern = Pattern.compile("^f?\\s*=?\\s*([+-]?\\d*\\D\\d.*)+\\s*-\\s*>\\s*(max|min)");
        Matcher matcher = pattern.matcher(equation);
        if (!matcher.find()) {
//            System.out.println("Matcher can't find equation");
            return null;
        }


//        System.out.println(matcher.groupCount());
//        for (int i = 0; i <= matcher.groupCount(); i++)
//            System.out.println("group [" + i + "]: " +matcher.group(i));

        result.equationVariables = findVariables(matcher.group(1));
        switch (matcher.group(2).toLowerCase()) {

            case "max": {
                result.type = EquationType.TENDS_MAX;
                break;
            }
            case "min": {
//                result.type = EquationType.TENDS_MIN;
                result.maximize();
                result.type = EquationType.TENDS_MAX;
                break;
            }
        }

//        System.out.println("result: " + result.toString());

        return result;
    }

    private static Map<String, Variable> findVariables(String variables){
        Map<String, Variable> result = new HashMap<>();

//        int iterations = 0;

        String vars = variables.replaceAll(" ", "");

        Pattern pattern1 = Pattern.compile("(([+-])?(\\d*)?(\\D*)(\\d*)).*");

        while(!vars.isEmpty()) {
            Matcher matcher1 = pattern1.matcher(vars);

            /**
             1 - (+|-|null)
             2 - variable modifier
             3 - variable letter
             4 - variable count
             */

            if (matcher1.find()) {
//                System.out.println(matcher1.groupCount());
//                for (int i = 0; i <= matcher1.groupCount(); i++)
//                    System.out.println("group [" + i + "]: " + matcher1.group(i));

               String sign, letter, modifierStr;
               int modifier, count;

               sign = matcher1.group(2);
               modifierStr = matcher1.group(3);
               letter = matcher1.group(4);
               count = Integer.valueOf(matcher1.group(5));

               if(modifierStr == null || modifierStr.isEmpty())
                   modifier = 1;
               else
                   modifier = Integer.valueOf(modifierStr);

               if(sign != null && sign.equals("-"))
                   modifier *= -1;

               result.put(letter + count, new Variable(modifier, 1));

               vars = vars.replace(matcher1.group(1), "");
            }else
                break;
        }

        return result;
    }

    public static boolean canParse(String targetFunction){
        Pattern pattern = Pattern.compile("^f?\\s*=?\\s*([+-]?\\d*\\D\\d.*)+\\s*-\\s*>\\s*(max|min)");
        Matcher matcher = pattern.matcher(targetFunction);
        return matcher.find();
    }

    public static Map.Entry<String, Variable> findFirstVariable(String variable) {
        String vars = variable.replaceAll(" ", "");

        Pattern pattern = Pattern.compile("(([+-])?(\\d*)?(\\D*)(\\d*)).*");
        Matcher matcher = pattern.matcher(vars);

        if (!matcher.find())
            return null;

        String sign, letter, modifierStr;
        int modifier, count;

        sign = matcher.group(2);
        modifierStr = matcher.group(3);
        letter = matcher.group(4);
        count = Integer.valueOf(matcher.group(5));

        if (modifierStr == null || modifierStr.isEmpty())
            modifier = 1;
        else
            modifier = Integer.valueOf(modifierStr);

        if (sign != null && sign.equals("-"))
            modifier *= -1;

        return new AbstractMap.SimpleEntry<>(letter + count, new Variable(modifier, 1));
    }

    public enum EquationType{
        TENDS_MAX, TENDS_MIN
    }

}
