package ru.popov.coursework.math.optimization.method.linear.simplex.data;

import ru.popov.coursework.math.data.Coordinate;
import ru.popov.coursework.math.data.Variable;
import ru.popov.coursework.math.optimization.method.linear.simplex.SimplexOperations;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SimplexTable {

    private List<String> column, line, nonBasicVariableNames;
    private List<Variable> evaluations;
    private Coordinate mainElementCoordinate, tableSize;
    private Map<String, Variable> table;

    public SimplexTable(){
        table = new HashMap<>();
        column = new ArrayList<>();
        line = new ArrayList<>();
        evaluations = new ArrayList<>();
        tableSize = new Coordinate(0, 0);
    }

    public void setColumn(List<String> column){
        this.column = new ArrayList<>(column);
    }

    public void setLine(List<String> line){
        this.line = new ArrayList<>(line);
        this.nonBasicVariableNames = new ArrayList<>(line);
    }

    public List<String> getNonBasicVariableNames(){
        return nonBasicVariableNames;
    }

    public List<String> getColumn(){return column;}

    public List<String> getLine(){return line;}

    public void setElement(Coordinate coordinate, Variable variable){
        if(coordinate.getX() >= tableSize.getX())
            tableSize.setX(coordinate.getX() + 1);
        if(coordinate.getY() >= tableSize.getY())
            tableSize.setY(coordinate.getY() + 1);

        table.put(coordinate.toString(), variable);
    }

    public void setElement(int x, int y, Variable variable){
        setElement(new Coordinate(x, y), variable);
    }

    public Variable getElement(Coordinate coordinate){
        return table.get(coordinate.toString());
    }

    public Variable getElement(int x, int y){
        return getElement(new Coordinate(x, y));
    }

    public void setEvaluations(List<Variable> variables){
        evaluations = variables;
    }

    public void setEvaluation(int position, Variable variable){
        if(position >= evaluations.size()){
            for(int i = evaluations.size(); i < position; i++)
                evaluations.add(null);
            evaluations.add(position, variable);
        }else{
            evaluations.set(position, variable);
        }
    }

    public Variable getEvaluation(int position){
        return evaluations.get(position);
    }

    public List<Variable> getEvaluations(){
        return evaluations;
    }

    public Coordinate getTableSize(){
        return tableSize;
    }

    public void setMainElementCoordinate(Coordinate coordinate){
        if(coordinate.getX() < tableSize.getX() && coordinate.getY() < tableSize.getY())
            mainElementCoordinate = coordinate;
    }

    public void setMainElementCoordinate(int x, int y){
        setMainElementCoordinate(new Coordinate(x, y));
    }

    public Coordinate getMainElementCoordinate(){
        if(mainElementCoordinate == null)
            findMainElementCoordinate();
        return mainElementCoordinate;
    }

    public Variable getMainElementValue(){
        return getElement(getMainElementCoordinate());
    }

    public Variable calculateValue(Coordinate requiredValue){
        return SimplexOperations.calculateValue(requiredValue, mainElementCoordinate, this);
    }

    public Variable calculateValue(int x, int y){
        return calculateValue(new Coordinate(x, y));
    }

    public Coordinate findMainElementCoordinate(){
        return mainElementCoordinate = SimplexOperations.findMainElementCoordinate(this);
    }

    @Override
    public String toString() {
        String defaultFormat = "%8s",
                smallFormat = "%4s";
        StringBuilder result = new StringBuilder();

        result.append("Main element: ");
        result.append(getMainElementValue().getFractionStr());
        result.append(" (");
        result.append(getMainElementCoordinate().toString());
        result.append(")");

        result.append("\n");
        result.append("Table: ");
        result.append("\n");
        result.append(String.format(smallFormat, ""));
        for(String s:line){
            result.append(String.format(defaultFormat, s));
        }
        result.append("\n");

        List<List<Variable>> vars = new ArrayList<>();

        for(int i = 0; i < getTableSize().getX(); i++) {
            List<Variable> var = new ArrayList<>();
            for (int j = 0; j < getTableSize().getY(); j++){
                Variable variable = getElement(i, j);
                var.add(variable);
            }
            vars.add(var);
        }

        for(int i = 0; i < vars.size(); i++) {
            for (int j = -1; j < vars.get(i).size(); j++) {
                if(j == -1) {
                    result.append(String.format(smallFormat, getColumn().get(i)));
                    continue;
                }
                result.append(String.format(defaultFormat, /*"[" + i + ", " + j + "]" + */vars.get(i).get(j).getFractionStr()));
            }
            result.append("\n");
        }
        result.append(String.format(smallFormat, "f"));
        for(Variable variable:evaluations){
            result.append(String.format(defaultFormat,variable.getFractionStr()));
        }
        return result.toString();
    }

    public static void log(String msg){
        System.out.println(msg);
    }

}
