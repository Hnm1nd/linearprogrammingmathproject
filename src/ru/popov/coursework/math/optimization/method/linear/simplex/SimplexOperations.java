package ru.popov.coursework.math.optimization.method.linear.simplex;

import ru.popov.coursework.math.data.CanonicalForm;
import ru.popov.coursework.math.data.Coordinate;
import ru.popov.coursework.math.data.Limitation;
import ru.popov.coursework.math.data.Variable;
import ru.popov.coursework.math.optimization.method.linear.simplex.data.SimplexTable;

import java.util.List;
import java.util.Map;

public class SimplexOperations {

    public static Variable calculateValue(Coordinate requiredValueCoordinate, Coordinate mainElementCoordinate, SimplexTable table){
        if(requiredValueCoordinate.getX() == mainElementCoordinate.getX() && requiredValueCoordinate.getY() == mainElementCoordinate.getY())
            return Variable.divide(new Variable(1, 1), table.getMainElementValue());
        if(requiredValueCoordinate.getX() == mainElementCoordinate.getX()){
            return calculateValueLine(table.getElement(requiredValueCoordinate), table.getElement(mainElementCoordinate));
        }
        if(requiredValueCoordinate.getY() == mainElementCoordinate.getY()){
            return calculateValueColumn(table.getElement(requiredValueCoordinate), table.getElement(mainElementCoordinate));
        }

        return calculateValue(table.getElement(mainElementCoordinate),
                table.getElement(requiredValueCoordinate),
                table.getElement(new Coordinate(mainElementCoordinate.getX(), requiredValueCoordinate.getY())),
                table.getElement(new Coordinate(requiredValueCoordinate.getX(), mainElementCoordinate.getY())));
    }

    public static Variable calculateValue(Variable rs, Variable ij, Variable rj, Variable is){
        return Variable.divide(Variable.minus(Variable.multiply(rs, ij), Variable.multiply(rj, is)), rs);
    }

    public static Variable calculateValueColumn(Variable requiredValue, Variable mainElement){
        Variable var = Variable.divide(requiredValue, mainElement);
        return new Variable(var.getNumerator() * -1, var.getDenominator());
    }

    public static Variable calculateValueLine(Variable requiredValue, Variable mainElement){
        return Variable.divide(requiredValue, mainElement);
    }

    public static Coordinate findMainElementCoordinate(SimplexTable table){
        Coordinate tableSize = table.getTableSize();

        Variable minColumnVariable = table.getEvaluation(0),
                minLineVariable = null;
        int minColumnValueIndex = 0,
                minLineValueIndex = 0;

        /*
         * Calculating minimal column index
         */
        for(int i = 0; i < table.getEvaluations().size() - 1; i++){
            Variable variable = table.getEvaluation(i);
            if(Variable.isLess(variable, minColumnVariable)){
                minColumnValueIndex = i;
                minColumnVariable = variable;
            }
        }

        for(int j = 0; j < tableSize.getX(); j++) {
            Variable a = table.getElement(j, tableSize.getY() - 1), b = table.getElement(j, minColumnValueIndex);
            if(a.getNumerator() < 0 || b.getNumerator() == 0)
                continue;
            minLineVariable = Variable.divide(a, b);
            minLineValueIndex = j;
            break;
        }

        if(minLineVariable == null) {
            return null;
        }

        /*
         * Calculating minimal coefficient limitation/variable.
         * Variable > 0
         */
        for(int j = 1; j < tableSize.getX(); j++){
            Variable variable = table.getElement(j, minColumnValueIndex), limitation = table.getElement(j, tableSize.getX() - 1);

            if(variable.getNumerator() < 0 || limitation.getNumerator() == 0) {
                continue;
            }

            Variable divideResult = Variable.divide(limitation, variable);

            if(Variable.isLess(divideResult, minLineVariable)){
                minLineValueIndex = j;
                minLineVariable = divideResult;
            }
        }

        return new Coordinate(minLineValueIndex, minColumnValueIndex);
    }

    public static SimplexTable makeSimplexTable(CanonicalForm canonicalForm) {
        SimplexTable table = new SimplexTable();
        table.setColumn(canonicalForm.getBasicVariableNames());
        table.setLine(canonicalForm.getVariableList());

        return makeSimplexTableEvaluations(makeSimplexTableElements(table, canonicalForm), canonicalForm);
    }

    public static SimplexTable makeSimplexTableElements(SimplexTable table, CanonicalForm canonicalForm){

        List<Limitation> limitations = canonicalForm.getLimitations();

        for(int i = 0; i < limitations.size(); i++){

            int j = 0;

            for(Map.Entry<String, Variable> variableEntry:limitations.get(i).getLimitationVariables().entrySet()){
                if(canonicalForm.getBasicVariableNames().contains(variableEntry.getKey()))
                    continue;
                table.setElement(i, j, variableEntry.getValue());
                j++;
            }
            table.setElement(i, j, limitations.get(i).getValue());

        }

        return table;
    }

    public static SimplexTable makeSimplexTableEvaluations(SimplexTable table, CanonicalForm canonicalForm){
        for(int i = 0; i < canonicalForm.getVariableList().size(); i++) {
            Variable result = new Variable(0, 1);
            for (int j = 0; j < canonicalForm.getLimitations().size(); j++) {

                Variable coefficient = canonicalForm.getVariableCoefficientByName(canonicalForm.getBasicVariable(j)),
                element = table.getElement(j, i);
                result = Variable.plus(result, Variable.multiply(coefficient, element));

            }
            Variable coefficient = canonicalForm.getVariableCoefficientByName(canonicalForm.getVariableName(i));
            Variable result1 = Variable.minus(result, coefficient);
            table.setEvaluation(i, result1);
        }

        int i = canonicalForm.getVariableList().size();
        Variable result = new Variable(0, 1);
        for (int j = 0; j < canonicalForm.getLimitations().size(); j++) {

            Variable coefficient = canonicalForm.getVariableCoefficientByName(canonicalForm.getBasicVariable(j)),
                    element = table.getElement(j, i);
            result = Variable.plus(result, Variable.multiply(coefficient, element));
        }
        table.setEvaluation(i, result);
        return table;
    }

    public static boolean isOptimal(SimplexTable table){
        for(int i = 0; i < table.getEvaluations().size(); i++){
            Variable variable = table.getEvaluation(i);
            if(variable.getNumerator() < 0) {
                return false;
            }
        }
        return true;
    }

}
