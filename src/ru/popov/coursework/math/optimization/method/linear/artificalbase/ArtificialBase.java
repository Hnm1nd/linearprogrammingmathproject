package ru.popov.coursework.math.optimization.method.linear.artificalbase;

import ru.popov.coursework.math.data.AlgorithmOutputData;
import ru.popov.coursework.math.data.Coordinate;
import ru.popov.coursework.math.data.Objective;
import ru.popov.coursework.math.data.Variable;
import ru.popov.coursework.math.optimization.method.linear.LinearMethod;
import ru.popov.coursework.math.optimization.method.linear.artificalbase.data.ArtificialBaseVariable;
import ru.popov.coursework.math.optimization.method.linear.simplex.Simplex;
import ru.popov.coursework.math.optimization.method.linear.simplex.data.SimplexTable;

/**
 * Created by User on 31.03.2017.
 */
public class ArtificialBase extends LinearMethod<Objective, ArtificialBaseVariable, SimplexTable, AlgorithmOutputData> {

    public ArtificialBase(){
        super();
    }

    public ArtificialBase(Objective objective){
        super(objective);
    }

    @Override
    protected SimplexTable makeInitialParams(Objective objective) {
        return null;
    }

    @Override
    protected SimplexTable onIteration(SimplexTable table) {
        return null;
    }

    @Override
    protected ArtificialBaseVariable onCount(Coordinate coordinate, SimplexTable table) {
        return null;
    }

    @Override
    protected boolean canFinish(SimplexTable table) {
        return false;
    }

}
