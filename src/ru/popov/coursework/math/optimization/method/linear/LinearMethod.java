package ru.popov.coursework.math.optimization.method.linear;

import ru.popov.coursework.interfaces.AlgorithmListener;
import ru.popov.coursework.interfaces.OnCount;
import ru.popov.coursework.interfaces.OnEvent;
import ru.popov.coursework.math.data.Coordinate;
import ru.popov.coursework.math.data.Objective;
import ru.popov.coursework.math.data.Variable;
import ru.popov.coursework.math.optimization.method.linear.simplex.data.SimplexTable;

public abstract class LinearMethod<StartParams extends Objective, CountResult extends Variable, Table extends SimplexTable, Result> {

    private StartParams params;
    private AlgorithmListener<Table, Result> algorithmListener;
    private OnCount<Coordinate, Table, CountResult> onCount;
    private OnEvent<Object> eventListener;
    private Result result;

    private int iterationsCount = 0;

    public LinearMethod(){}

    public LinearMethod(StartParams params){
        setStartParameters(params);
    }

    public void count(){
        if(params != null)
            onStart(params);
        else
            algorithmListener.onError(new NullPointerException("Input parameters not set"));
    }

    private void onStart(StartParams t) {
        if(algorithmListener != null)
            algorithmListener.onStart();

        Table table = makeInitialParams(t);
        iterationLog(table);
        while(result == null && !canFinish(table))
            table = nextIteration(table);

        onFinished(result);
    }

    private void onFinished(Result e) {
        if(algorithmListener != null)
            algorithmListener.onFinished(e);
    }

    private Table nextIteration(Table table){
        iterationsCount++;
        Table result = onIteration(table);
        iterationLog(result);
        return result;
    }

    private void iterationLog(Table table){
        if(algorithmListener != null)
            algorithmListener.onIteration(iterationsCount, table);
    }

    public void setStartParameters(StartParams t){
        params = t;
    }

    public StartParams getStartParameters(){
        return params;
    }

    protected void setResult(Result result){
        this.result = result;
    }

    protected CountResult countValue(Coordinate coordinate, Table table){
        if(onCount != null)
            return onCount.onCount(coordinate, table);
        return onCount(coordinate, table);
    }

    public void setListener(AlgorithmListener<Table, Result> listener) {
        this.algorithmListener = listener;
    }

    public void setEventListener(OnEvent<Object> eventListener){
        this.eventListener = eventListener;
    }

    protected void onEvent(int eventType, Object o){
        if(eventListener != null)
            eventListener.onEvent(eventType, o);
    }

    public void setOnCountMethod(OnCount<Coordinate, Table, CountResult> onCount){
        this.onCount = onCount;
    }

    protected abstract Table onIteration(Table table);

    protected abstract CountResult onCount(Coordinate inputCountParam1, Table inputCountParam2);

    protected abstract Table makeInitialParams(StartParams startParams);

    protected abstract boolean canFinish(Table table);
}
