package ru.popov.coursework.math.optimization.method.linear.simplex;

import ru.popov.coursework.math.data.*;
import ru.popov.coursework.math.optimization.method.linear.LinearMethod;
import ru.popov.coursework.math.optimization.method.linear.simplex.data.SimplexTable;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Simplex extends LinearMethod<Objective, Variable, SimplexTable, AlgorithmOutputData> {

    private List<String> targetVariableNames;

    public Simplex(){
        super();
    }

    public Simplex(Objective objective){
        super(objective);
    }

    @Override
    protected SimplexTable makeInitialParams(Objective objective) {
        targetVariableNames = new ArrayList<>(objective.getTargetFunction().getEquationVariables().keySet());
        return SimplexOperations.makeSimplexTable(CanonicalForm.makeCanonicalForm(objective));
    }

    @Override
    protected SimplexTable onIteration(SimplexTable table) {
        SimplexTable resultTable = new SimplexTable();

        Coordinate mainElementCoordinate = table.getMainElementCoordinate();
        if(mainElementCoordinate == null){
//            System.out.println("mainElementCoordinate == null");
            setResult(new AlgorithmOutputData(null));
            return null;
        }

        Coordinate tableSize = table.getTableSize();

        /*
         * Calculate values of matrix
         */
        for(int i = 0; i < tableSize.getX(); i++){
            for (int j = 0; j < tableSize.getY(); j++){
                resultTable.setElement(i, j, countValue(new Coordinate(i, j), table));
            }
        }

        /*
         * Calculate evaluations
         */
        for(int i = 0; i < table.getEvaluations().size(); i++){
            if(i != mainElementCoordinate.getX() - 1) {
                Variable variable = SimplexOperations.calculateValue(table.getMainElementValue(),
                        table.getEvaluation(i),
                        table.getElement(new Coordinate(mainElementCoordinate.getX(), i)),
                        table.getEvaluation(mainElementCoordinate.getY()));
                resultTable.setEvaluation(i, variable);
            } else {
                Variable variable = SimplexOperations.calculateValueColumn(table.getEvaluation(i), table.getMainElementValue());
                resultTable.setEvaluation(i, variable);
            }
        }

        /*
         * Swap line and column element
         */
        List<String> column = new ArrayList<>(table.getColumn()),
                line = new ArrayList<>(table.getLine());

        String buffer = column.get(mainElementCoordinate.getX());
        column.set(mainElementCoordinate.getX(), line.get(mainElementCoordinate.getY()));
        line.set(mainElementCoordinate.getY(), buffer);

        resultTable.setColumn(column);
        resultTable.setLine(line);

        return resultTable;
    }

    @Override
    protected Variable onCount(Coordinate coordinate, SimplexTable table) {
        Variable result;
        if(coordinate.getX() < table.getTableSize().getX()) {
            result = SimplexOperations.calculateValue(coordinate, table.getMainElementCoordinate(), table);
        }else {
            Coordinate mainElementCoordinate = table.getMainElementCoordinate();
            int index = coordinate.getX();
            if (index != mainElementCoordinate.getX() - 1) {
                result = SimplexOperations.calculateValue(table.getMainElementValue(),
                        table.getEvaluation(index),
                        table.getElement(new Coordinate(mainElementCoordinate.getX(), index)),
                        table.getEvaluation(mainElementCoordinate.getY()));
            } else {
                result = SimplexOperations.calculateValueColumn(table.getEvaluation(index), table.getMainElementValue());
            }
        }
        return result;
    }

    @Override
    protected boolean canFinish(SimplexTable table) {
        boolean isOptimal;
        if(isOptimal = SimplexOperations.isOptimal(table)) {
            Coordinate resultTableSize = table.getTableSize();
            Map<String, Variable> vars = new HashMap<>();
            for (int i = 0; i < table.getColumn().size(); i++) {
                if(targetVariableNames.contains(table.getColumn().get(i)))
                vars.put(table.getColumn().get(i), table.getElement(i, resultTableSize.getY() - 1));
            }
            vars.put("f", table.getEvaluation(table.getEvaluations().size() - 1));
            AlgorithmOutputData out = new AlgorithmOutputData(vars);
            setResult(out);
        }
        return isOptimal;
    }

}
