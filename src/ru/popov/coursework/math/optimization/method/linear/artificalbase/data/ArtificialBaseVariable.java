package ru.popov.coursework.math.optimization.method.linear.artificalbase.data;

import ru.popov.coursework.math.data.Variable;

/**
 * Created by User on 31.03.2017.
 */
public class ArtificialBaseVariable extends Variable{

    public ArtificialBaseVariable(int numerator, int denominator) {
        super(numerator, denominator);
    }

    @Override
    public String getFractionStr() {
        if (getDenominator() < 0)
            return "m";
        else
            return super.getFractionStr();
    }

}
