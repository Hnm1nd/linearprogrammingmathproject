package ru.popov.coursework.interfaces;

/**
 * Created by User on 25.03.2017.
 */
public interface OnEvent<E> {
    void onEvent(int eventType, E eventData);
}
