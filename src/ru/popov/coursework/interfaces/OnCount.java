package ru.popov.coursework.interfaces;

/**
 * Created by User on 31.03.2017.
 */
public interface OnCount<Input1, Input2, Result> {
    Result onCount(Input1 input1, Input2 input2);
}
