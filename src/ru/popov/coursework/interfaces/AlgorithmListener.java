package ru.popov.coursework.interfaces;

import ru.popov.coursework.math.optimization.method.linear.simplex.data.SimplexTable;

/**
 * Created by User on 24.03.2017.
 */
public interface AlgorithmListener<E extends SimplexTable, R> extends OnEvent<Object> {
    void onStart();
    void onIteration(int iterationsCount, E table);
    void onError(Throwable throwable);
    void onFinished(R result);
}
