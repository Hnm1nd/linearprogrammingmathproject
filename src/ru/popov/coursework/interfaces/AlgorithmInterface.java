package ru.popov.coursework.interfaces;

/**
 * Created by bronetemkin on 25.03.17.
 */
public interface AlgorithmInterface<StartParams, CountResult, InputCountParam1, InputCountParam2, Result> extends OnCount<InputCountParam1, InputCountParam2, CountResult> {
    void onStart(StartParams params);
    void onFinished(Result result);
}
